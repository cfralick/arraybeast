<?php
namespace ArrayBeast\Tests;

use ArrayBeast\ArrayBeast;

class ArrayBeastErrorTest extends \PHPUnit_Framework_TestCase
{

        /**
         * @expectedException PHPUnit_Framework_Error
         */
        public function testMapRequiresClosure()
        {
            $arrayBeast = new ArrayBeast();
            $arrayBeast->map();
        }
        /**
         * @expectedException PHPUnit_Framework_Error
         */
        public function testEachRequiresClosure()
        {
            $arrayBeast = new ArrayBeast();
            $arrayBeast->each();
        }
        /**
         * @expectedException PHPUnit_Framework_Error
         */
        public function testFilterRequiresClosure()
        {
            $arrayBeast = new ArrayBeast();
            $arrayBeast->filter();
        }
        /**
         * @expectedException PHPUnit_Framework_Error
         */
        public function testPurgeRequiresClosure()
        {
            $arrayBeast = new ArrayBeast();
            $arrayBeast->purge();
        }
        /**
         * @expectedException PHPUnit_Framework_Error
         */
        public function testMapIfRequiresClosure()
        {
            $arrayBeast = new ArrayBeast();
            $arrayBeast->mapIf();
        }
}
