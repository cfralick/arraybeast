<?php

namespace ArrayBeast\Tests;

use ArrayBeast\ArrayBeast;

class ArrayBeastTest extends \PHPUnit_Framework_TestCase
{
    public $sample_data;

    protected function setUp()
    {
        $this->sample_data = array(
            'key' => 'stringValue',
            'index' => 30,
            'property' => true,
            'identifier' => array(1, 2, 3),
            'innerarray' => array('str' => 'astring', 'num' => 10)
        );
    }

    public function provider()
    {
        $data = [];
        $size = rand(5, 40);
        for($i = 1; $i <= $size; $i++) {
            $data[(String) substr(md5(uniqid(rand())), 2,20)] = substr(md5(rand()), 1, 18);
        }
        return $data;
    }


    /**
     * @covers ArrayBeast\ArrayBeast::__construct
     * @covers ArrayBeast\ArrayBeast::getIterator
     * @covers ArrayBeast\ArrayBeast::keys
     * @covers ArrayBeast\ArrayBeast::values
     * @covers ArrayBeast\ArrayBeast::count
     */
    public function testInstantiation()
    {
        $this->assertInstanceOf('ArrayBeast\ArrayBeast', new ArrayBeast());
        $sample_data = $this->provider();
        $arrayBeast = new ArrayBeast($sample_data);
        $this->assertEquals(count($sample_data), $arrayBeast->count());
        $this->assertInstanceOf('\Iterator', $arrayBeast->getIterator());
        $this->assertInstanceOf('\SplFixedArray', $arrayBeast->values());
        $this->assertInstanceOf('\SplFixedArray', $arrayBeast->keys());
    }

    /**
     * @covers ArrayBeast\ArrayBeast::first
     */
    public function testFirstMethod()
    {
        $arrayBeast = new ArrayBeast();
        $this->assertFalse($arrayBeast->first(function($c, $k, $i) { return isset($c);}));
        $arrayBeast['value'] = true;
        $this->assertTrue($arrayBeast->first(function($c, $k, $i) { return $c === true;}));

    }

    /**
     * @covers ArrayBeast\ArrayBeast::each
     * @covers ArrayBeast\ArrayBeast::getArrayCopy
     * @covers ArrayBeast\ArrayBeast::filter
     * @covers ArrayBeast\ArrayBeast::purge
     * @covers ArrayBeast\ArrayBeast::flatten 
     */
    public function testMethodsThatChangeOriginalValues()
    {
        $arrayBeast = new ArrayBeast($this->provider());
        $original_values = $arrayBeast->getArrayCopy();
        
        $arrayBeast->each(function($item) { return [$item]; });
        $modified_values = $arrayBeast->getArrayCopy();
        
        $this->assertNotSame($original_values, $modified_values);
        $this->assertSame(array_keys($original_values), array_keys($modified_values));
        $arrayBeast->purge(function($x) { return !is_array($x) || preg_match('/[a-z][0-9][a-z]/', $x[0]);});
        $this->assertNotSame($modified_values, $arrayBeast->getArrayCopy());

        $flattened = $arrayBeast->flatten();
        foreach($flattened as $value) {
            $this->assertNotInternalType('array', $value);
        }
    }

    /**
     * @covers ArrayBeast\ArrayBeast::clear
     */
    public function testClear()
    {
        $arrayBeast = new ArrayBeast($this->provider());
        $this->assertGreaterThan(0, $arrayBeast->count());
        $arrayBeast->clear();
        $this->assertEquals(0, $arrayBeast->count());
    }
    /**
     * @covers ArrayBeast\ArrayBeast::shift
     */
    public function testShift()
    {
        $arrayBeast = new ArrayBeast($this->sample_data);
        $slice = $arrayBeast->shift('index');
        $this->assertSame(30, $slice);
    }
    
    /**
     * @covers ArrayBeast\ArrayBeast::select
     */
    public function testSelect()
    {
        $arrayBeast = new ArrayBeast($this->sample_data);
        $slice = $arrayBeast->select(['index']);
        $this->assertEquals(1, $slice->count());
    }

    /**
     * @covers ArrayBeast\ArrayBeast::reassign
     */
    public function testReassign()
    {
        $arrayBeast = new ArrayBeast($this->sample_data);
        $arrayBeast->reassign('property', 'item');
        $this->assertNull($arrayBeast['property']);
    }

    /**
     * @covers ArrayBeast\ArrayBeast::map
     * @covers ArrayBeast\ArrayBeast::mapIf
     */
    public function testMapping()
    {
        $arrayBeast = new ArrayBeast($this->provider());
        $arrayBeastMap = $arrayBeast->map(function($v) { return new ArrayBeast([$v]); });
        $this->assertInstanceOf('\ArrayBeast\ArrayBeast', $arrayBeastMap);
        foreach($arrayBeastMap as $key => $value) {
            $this->assertInstanceOf('\ArrayBeast\ArrayBeast', $value);
        }
        $original_count = $arrayBeast->count();
        $arrayBeastMapIf = $arrayBeast->mapIf(function($x) { return preg_match('/[a8ehgs]/', $x); }, function($v) { return new ArrayBeast([$v]); });
        $this->assertInstanceOf('\ArrayBeast\ArrayBeast', $arrayBeastMapIf);
        foreach($arrayBeastMapIf as $key => $value) {
            $this->assertInstanceOf('\ArrayBeast\ArrayBeast', $value);
        }
        $this->assertGreaterThanOrEqual($arrayBeastMapIf->count(), $original_count);
    }

    /**
     * @covers ArrayBeast\ArrayBeast::merge
     * @covers ArrayBeast\ArrayBeast::filter
     */
    public function testMergeAndFilter()
    {
        $initial_data = $this->provider();
        $merge_data = $this->provider();
        
        $arrayBeast = new ArrayBeast($initial_data);
        $arrayBeast->merge($merge_data);
        $this->assertSame(array_merge($initial_data, $merge_data), $arrayBeast->getArrayCopy());

        $pattern = '/[a-z][nvflasjoiewt0-9]/';
        
        $arrayBeastFiltered = $arrayBeast->filter(function($x) use ($pattern) { return preg_match($pattern, $x); });
        foreach($arrayBeastFiltered as $key => $value) {
            $this->assertRegExp($pattern, $value);
        }
    }

    /**
     * @covers ArrayBeast\ArrayBeast::pick
     */
    public function testPickSimpleKeySelect()
    {
        $arrayBeast = new ArrayBeast($this->sample_data);
        $result = $arrayBeast->pick(array('index'));
        $this->assertSame(30, $result['index']);
    }

    /**
     * @covers ArrayBeast\ArrayBeast::pick
     */
    public function testPickApplyClosure()
    {
        $arrayBeast = new ArrayBeast($this->sample_data);
        $result = $arrayBeast->pick(array('index' => function($i) { return $i * 2; }));
        $this->assertSame(60, $result['index']);
    }

}

