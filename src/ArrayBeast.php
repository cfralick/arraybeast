<?php

/*
 * ArrayBeast is an extension of \ArrayObject that has additional
 * PHP array-like behaviors. In most cases, ArrayBeast accepts a
 * \Closure to facilitate mapping and filtering behavior.
 */


/**
 * ArrayBeast main class
 *
 * @package arraybeast
 *
 */
class ArrayBeast extends \ArrayObject
{
    
    /**
     * Instatiate the object 
     *
     * @param array $data
     */
    public function __construct(array $data = array())
    {
        parent::__construct($data, 2);
    }

    /**
     * Remove and return a id => value pair
     *
     * @param string $id 
     * @access public
     */ 
    public function shift($id)
    { 
        $ret = $this->offsetGet($id);
        $this->offsetUnset($id);

        return $ret;
    }

    /**
     * Mutable - Empty the internal storage container
     */
    public function clear() 
    {
        $this->exchangeArray([]);
        return $this;
    }
    
    /**
     * Mutable - Assign an existing value to a new index and remove the old
     *
     * @param string $id the index to remove
     *
     * @param steing $key the new index to create
     */
    public function reassign($id, $key)
    {
        if(!$this->offsetExists($id)) {
            throw new \InvalidArgumentException(sprintf('Identifier "%s" is not defined', $id));
        }

        $this->offsetSet($key, $this->offsetGet($id));
        $this->offsetUnset($id);
        
        return $this;
    }

    /**
     * Mutable - Apply a callback to each value
     *
     * @param Closure $cb the function to apply
     */
    public function each(\Closure $cb)
    {
        foreach($this as $key => $value) {
            $this[$key] = $cb($value);
        }

        return $this;
    }

    /**
     * Return the first value on which the supplied callback returns true
     * 
     * @param \Closure $cb the callback filter
     * 
     * @return mixed the value accepted by the callback
     */
    public function first(\Closure $cb)
    {
        $itr = new \CallbackFilterIterator($this->getIterator(), $cb);
        $itr->rewind();
        return $itr->valid() ? $itr->current() : false;
    }
    
    
    /**
     * Immutable - Apply a callback to each value, adds the result to a new array
     *  which is used to generate a new ArrayBeast.
     *
     * @param Closure $cb the function to apply
     */
    public function map(\Closure $cb)
    {
        $itr = [];
        foreach($this as $k => $v){
            $itr[$k] = $cb($v);
        }
        return new static($itr);
    }

    /**
     * Apply an initial callback to each value as a filter,
     * then apply a second callback to each value that passes the filter 
     * and return a new ArrayBeast containing the results.
     *
     * @param Closure $cb the function to apply as a filter
     *
     * @param Closure $fn the function to apply as modifier
     */
    public function mapIf(\Closure $cb, \Closure $fn)
    {
        $itr = $this->filter($cb);
        $itr->each($fn);
        return new static($itr);
    }


    /**
     * Flatten the existing values into a one-dimensional array and
     * return a new ArrayBeast instantiated with the flattened array.
     */
    public function flatten()
    {
        $itr = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($this->getArrayCopy()));
        return new self(iterator_to_array($itr));
    }


    /**
     * Apply a callback to each value as a filter and return a new ArrayBeast
     * containing the values that met the filter constraint.
     * 
     * @param Closure $cb the callback to apply as filter
     */
    public function filter(\Closure $cb)
    {
        return new self(iterator_to_array(new \CallbackFilterIterator($this->getIterator(), $cb)));
    }


    /**
     * Mutable -- Apply a callback to each value as a filter; values that satisfy the filter requirement 
     * are removed from the original ArrayBeast.
     *
     * @param Closure $cb the function to apply as filter
     */
    public function purge(\Closure $cb)
    {
        $itr = new \CallbackFilterIterator($this->getIterator(), $cb);
        foreach($itr as $key => $value) {
            $this->offsetUnset($key);
        }

        return $this;
    }


    /**
     * Return an array filtered to include only the key => value pairs specified in $keys
     *
     * @param array $keys the desired keys
     */
    public function select(array $keys = array())
    {
        $filtered = $this->filter(function($current, $key, $iterator) use ($keys) {
                return in_array($key, $keys);
        });

        return new self($filtered);
    }


    /**
     * Create an array consisting of the keys of the original array
     */
    public function keys()
    {
        return \SplFixedArray::fromArray(array_keys($this->getArrayCopy()));
    }


    /**
     * Create an array consisting of the values of the original array
     */
    public function values()
    {
        return \SplFixedArray::fromArray(array_values($this->getArrayCopy()));
    }


    /**
     * Merge an array with the existing $data array
     *
     * @param array $array the additional values to merge
     */
    public function merge(array $array)
    {
        $destination_beast = new static($array);
        foreach($this as $key => $value) {
            $destination_beast[$key] = $value;
        }
        return $destination_beast;
    }

    /**
     *   ** THIS METHOD IS UNFINISHED **
     *  
     * Filters the $data using an array of selectors and returns a new array.
     * Original values & and the calling \ArrayBeast are unchanged.
     * Several kinds of actions can be defined in the $selector parameter.
     *
     * @param array $selector the array of filters
     * 
     * The $selector array may contain several different kinds of values:
     * Closures - closures should accept a single argument and return a scalar value
     * Arrays - arrays with integer keys will be used to extract a value from a sub-array
     *          arrays with string keys will extract a value and execute an offset reassignment
     * Strings - offsets to select from the $data array
     * 
     *
     * @return array the filtered result
     */
    public function pick(array $selector)
    {
        $result = [];
        
        foreach($selector as $key => $value) {
            // if current $selector is not a $key => $value pair, set the $key = $value to avoid integer keys in result
            if(is_int($key)) { 
                $key = $value;
            }
           
            // check that offset specified by the selector exists
            if(!isset($this[$key])) {
                continue;
            }
           
            // if the current $selector value is a callable, call, passing $this[$key] as the argument.
            if(is_callable($value)) {
                $result[$key] = $value($this[$key]);
            } 

            /*
             * Attempts to extract an inner value from an offset that is an array:
             */
            elseif(is_array($value) && is_array($this[$key])) {
                $result_key = is_string(key($value)) ? key($value) : $key;
                // if the inner value exists, set $result[$key] to it. Otherwise set it false.
                $result[$result_key] = isset($this[$key][current($value)]) ? $this[$key][current($value)] : false;
            } else {
                // if selector is just a plain string identifier, set $result[$k] to $target[$k]
                $result[$key] = $this[$key];
            }
        }
        // remove any empty keys set in process & return
        return array_filter($result);
    }
}
