### ArrayBeast
***

__An extension of PHP's ArrayObject with Map, Filter, and Merge__

*(disclaimer: work in progress & 1st github contribution)*

#### Overview
* Mimics native PHP behavior of:
  *  array_keys  
  *  array_values
  *  array_map
  *  array_merge

* Additional methods:
  * flatten 
  * reassign
  * select & purge
  * pick - applies a complex filter (which may contain index & value conversions, and closures) to the $data array


* To-do:
  * rewrite unit tests
  * refactor \ArrayBeast::pick
